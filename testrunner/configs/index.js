let deviceConfig = require('./device.json');
let serverConfig = require('./server.json');
let testsConfig = require('./tests.json');
let testPageList = require(testsConfig['testem-json-path'])['test_page'];
let ip = require('ip');

testsConfig['test_page'] = testPageList.map(function (pageUrl) {
    return 'http://' + testsConfig['sandbox-url'] + pageUrl;
});
serverConfig['adress'] = `http://${ip.address()}:${serverConfig.port}`;

module.exports = {
    device: deviceConfig,
    server: serverConfig,
    tests: testsConfig
};