﻿let AndroidPlayer = require('./lib/AndroidPlayer.js');
let Thread = require('./lib/Thread.js');
let Logger = require('./lib/Logger.js');
let ReportsArray = require('./models/ReportsArray.js');
let configs = require('./configs');
let server = require('./lib/server.js');
let reportStore = new ReportsArray(),
    player = new AndroidPlayer(configs.device, configs.tests, configs.server),
    thread = new Thread(player, [runTest, null]);

function runTest () {
    this.unblock();
    this.openCurrentUrlInCurrentBrowser();
}

server.run(configs.server);
player.enable();
thread.runAfterTruthfulConditionWithDelay(player.isLoaded, 10 * 1000);

server.emitter.on('reportupdate', (reportString) => {
    reportStore.push(reportString, player.browsers.getCurrent());
    if (!player.urls.hasFirstItem()) {
        if (!player.browsers.hasNext()) {
            Logger.warning(reportStore);
            process.exit('tests end');
            return;
        } else {
            player.browsers.next();
            player.urls.reset();
        }
    }
    player.openCurrentUrlInCurrentBrowser();
});