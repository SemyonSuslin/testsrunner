class FailsArray {
    constructor (val) {
        this.value = val;
    }
    toString() {
        let res = '';
        for (let i in this.value) {
            res += this.value[i];
        }
        return res;
    }
}
module.exports = FailsArray;