let Fail = require('./Fail.js'),
    FailsArray = require('./FailsArray.js');

class Report {
    constructor(reportJsonString, number) {
        this.valueObj = JSON.parse(reportJsonString);
        this.userAgent = this.valueObj.userAgent;
        this.currentUrl = this.valueObj.currentUrl;
        this.failsArray = this.getFailsArray(this.valueObj.fails);
        this.internalNumber = number;
    }

    getFailsArray(fails) {
        let res = [];
        for (var i in fails) {
            res.push(new Fail(fails[i], i));
        }
        return new FailsArray(res);
    }

    toString() {
        return `
        [${this.currentUrl}]
            userAgent: ${this.userAgent}
            fails: [
                ${this.failsArray}
            ]
        `;
    }
}

module.exports = Report;
