let Fail = require('./Fail.js'),
    Report = require('./Report.js'),
    FailsArray = require('./FailsArray.js');

class ReportsArray {
    constructor() {
        this.value = {};
    }
    push (item, browserName) {
        if(this.value[browserName]) this.value[browserName].push(new Report(item, this.value.length));
        else this.value[browserName] = [];
    }

    toString() {
        let res = '';
        for (let i in this.value) {
            res += `${i}
            ${this.value[i]}`;
        }
        return res;
    }
}

module.exports = ReportsArray;
