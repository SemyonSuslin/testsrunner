class Fail {
    constructor (item, number) {
        this.internalNumber = number;
        this.description = item.description;
        this.id = item.id;
        this.parentDescription = item.parentDescription;
    }
    toString() {
        return `
            [${this.internalNumber}]
                description: ${this.description}
                id: ${this.description}
                parentDescription: ${this.parentDescription}
        `;
    }
}
module.exports = Fail;