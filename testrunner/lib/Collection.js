class Collection {
    constructor(list) {
        this.value = list;
        this.length = list.length;
        this.currentIndex = 0;
    }

    next() {
        let result;
        this.currentIndex++;
        if (this.hasNext()) {
            result = this.getCurrent();
        } else {
            result = null;
        }
        return result;
    }

    hasNext() {
        return this.currentIndex + 1 < this.length
    }

    get(index) {
        return this.value[index]
    }

    getCurrent() {
        return this.get(this.currentIndex);
    }
}

module.exports = Collection;