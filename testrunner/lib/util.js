function cloneArray(arr) {
    let res = [];
    for (let i in arr) res[i] = arr[i];
    return res;
}

module.exports = {
    cloneArray: cloneArray
};