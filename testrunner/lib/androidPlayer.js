﻿let Thread = require('./Thread.js'),
    Collection = require('./Collection.js'),
    Logger = require('./Logger.js'),
    Queue = require('./Queue.js');

let cmd = require('node-cmd'),
    ip = require('ip'),
    isLoad = false;

const isLoadedRegex = /(?:[0-9]{1,3}\.){3}[0-9]{1,3}:[0-9]{1,4}\s(device)/;

module.exports = class AndroidPlayer {
    constructor(deviceConfig, testsConfig, serverConfig) {
        this.player = deviceConfig.player;
        this.adb = deviceConfig.adb;
        this.nameDevice = deviceConfig.name;
        this.urls = new Queue(testsConfig.test_page);
        this.browsers = new Collection(['Chrome', 'Firefox'/*, 'StockBrowser'*/]);
        this.reporterAdress = serverConfig.adress;
    }
    enable() {cmd.run(`"${this.player}" --name ${this.nameDevice}`);}
    unblock() {cmd.run(`${this.adb} shell input keyevent 82`);}
    isLoaded() {
        if(isLoad) return true;
        cmd.get(`"${this.adb}" devices`, function (data) {
            if (data.search(isLoadedRegex) > -1) isLoad = true;
        });
    }
    openUrlInBrowser(browserName, url) {
        Logger.important(`browser ${browserName} open on ${url}`);
        this['openUrlIn' + browserName](url);
    }
    openCurrentUrlInCurrentBrowser () {
        this.openUrlInBrowser(this.browsers.getCurrent(), `${this.urls.shift()}?is_log=true~adress=${this.reporterAdress}`);
    }
    openUrlInChrome(url) {
        cmd.run(`${this.adb} shell am start -n com.android.chrome/com.google.android.apps.chrome.Main -d "${url}" --activity-clear-task`);
    }
    openUrlInFirefox(url) {
        cmd.run(`${this.adb} shell am start -a android.intent.action.VIEW -n org.mozilla.firefox/.App -d "${url}"`);
    }
    openUrlInStockBrowser(url) {
        cmd.run(`${this.adb} shell am start -a android.intent.action.VIEW -d "${url}"`);
    }
};