let util = require('./util.js');

module.exports = class Queue {
    constructor(list) {
        this.value = list;
        this.internalValue = util.cloneArray(this.value);
    }

    shift() {
        let result;
        if (this.hasFirstItem()) {
            result = this.value.shift();
        } else {
            result = null;
        }
        return result;
    }

    hasFirstItem() {
        return !!this.value.length
    }

    reset() {
        this.value = util.cloneArray(this.internalValue);
    }
};