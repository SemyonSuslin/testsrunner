﻿let http = require('http');
let EventEmitter = require('events');
let Logger = require('./Logger.js');
let emitter = new EventEmitter();
let ip = require('ip');

module.exports = {
    run: function (config) {
        var handler = function (req, res) {
            if (req.method === 'OPTIONS') {
                var headers = {};
                headers["Access-Control-Allow-Origin"] = "*";
                headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
                headers["Access-Control-Allow-Credentials"] = false;
                headers["Access-Control-Max-Age"] = '86400'; // 24 hours
                headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept";
                res.writeHead(200, headers);
                res.end();
            } else {
                let internalResult = {};

                req.on('data', function (data) {
                    internalResult = data;
                });
                req.on('error', function (err) {
                    console.error(err.stack);
                });
                req.on('end', function () {
                    emitter.emit('reportupdate', internalResult);
                });
            }
        };
        var server = http.createServer();
        server.addListener('request', handler);
        server.listen(config.port);
        Logger.important(`server run:${config.adress}`);
    },
    emitter: emitter
};