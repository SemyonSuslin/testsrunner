let Logger = require('./Logger.js');
const FREQUENCY = 50;

class Thread {
    constructor(contextCallback, callbackWithParamsList) {
        this.contextCallback = contextCallback;
        [this.callback, this.callbackParams] = callbackWithParamsList;
    }

    runAfterDelay(delay = 0) {
        let me = this,
            startTime = Date.now(),
            timer = setInterval(function () {
                if (delay < Date.now() - startTime) {
                    clearTimeout(timer);
                    me.callback.call(me.contextCallback, me.callbackParams);
                }
            }, FREQUENCY);
    }

    runAfterTruthfulConditionWithDelay(condition, delay = 0) {
        let me = this,
            timer = setInterval(function () {
                if (condition.call(me.contextCallback)) {
                    clearTimeout(timer);
                    Logger.important(`function <${condition.name}> returned 'true'; <${me.callback.name}> will call after delay: ${delay}ms`);
                    me.runAfterDelay(delay);
                }
            }, FREQUENCY);
    }
}

module.exports = Thread;