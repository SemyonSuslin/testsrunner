var colors = require('colors/safe');

class Logger {
    static print (info) {
        console.log(info)
    }
    static important (info) {
        console.log(colors.green(info));
    }
    static warning (info) {
        console.log(colors.red(info));
    }
    static error (info) {
        console.log(colors.rainbow(info));
    }
    static printAdress (info) {
        console.log(colors.rainbow(info));
    }
}

module.exports = Logger;
